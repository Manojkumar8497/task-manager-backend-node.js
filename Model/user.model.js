const mongoose = require('mongoose');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');

// JWT secret
const jwtSecret = process.env.JWT_SECRET_KEY;

const UserSchema = new mongoose.Schema({
    email: { type: String, minlength: 3, required: true, unique: true },
    password: { type: String, minlength: 8, required: true },
    sessions: [
        {
            token: { type: String, required: true },
            expiresAt: { type: String, required: true },
            device: { type: Object, required: false }
        }
    ]
}, { timestamps: true });

// Instanse Methods
UserSchema.methods.toJSON = function () {
    const user = this;
    const userData = user.toObject();

    // Return the document data without the password and session
    return _.omit(userData, ['password', 'sessions']);
}

// Create the json webtoken when the user logged in
UserSchema.methods.generateAccessAuthToken = function () {
    const user = this;
    return new Promise((resolve, reject) => {
        jwt.sign(
            { _id: user._id.toHexString() },
            jwtSecret,
            { expiresIn: '30m' },
            (err, token) => {
                if (err) {
                    reject(err);
                }
                resolve(token);
            }
        )
    })
}

// Generate a Refresh token
UserSchema.methods.generateRefreshAuthToken = function () {
    return new Promise((resolve, reject) => {
        crypto.randomBytes(64, (err, buf) => {
            if (err) {
                return reject(err);
            }
            let token = buf.toString('hex');
            return resolve(token);
        });
    })
}

// To create a session
UserSchema.methods.createSession = function (deviceName = 'Unknown', deviceType = 'Other') {
    let user = this;

    const device = {
        name: deviceName,
        type: deviceType
    }

    return user.generateRefreshAuthToken()
        .then((refreshToken) => {
            return saveSessionToDatabase(user, refreshToken, device);
        })
        .then((refreshToken) => {
            return refreshToken;
        })
        .catch(err => {
            return Promise.reject('Failed to save the session to database: ' + err);
        });
}

// To save the sessions to database
let saveSessionToDatabase = (user, refreshToken, device) => {
    return new Promise((resolve, reject) => {
        let expiresAt = generateRefreshTokenExpiresAt();
        user.sessions.push({ 'token': refreshToken, expiresAt, device });
        user.save()
            .then(() => {
                return resolve(refreshToken);
            })
            .catch(err => {
                return reject(err);
            })
    });
}

// To generate refresh token expiresAt
let generateRefreshTokenExpiresAt = () => {
    let daysUntilExpires = '10';
    let secondsUntilExpires = ((daysUntilExpires * 24) * 60) * 60;
    return ((Date.now() / 1000) + secondsUntilExpires);
}

// To check the refresh token is experied or not
UserSchema.statics.hasRefreshTokenExpired = (expiresAt) => {
    let seconds = Date.now() / 1000;
    if (expiresAt > seconds) {
        return false;
    }
    else {
        return true;
    }
}

/** Model methods */

// To find the user details by id and token
UserSchema.statics.findByIdAndToken = function (_id, token) {
    let user = this;
    return user.findOne({ _id, 'sessions.token': token });
}

// To find the user by email and password
UserSchema.statics.findByCredentials = function (email, password) {
    let User = this;
    return User.findOne({ email }).then(user => {
        if (!user) {
            return Promise.reject({ message: 'User not found' });
        }
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, success) => {
                if (success) {
                    resolve(user);
                }
                else {
                    reject({ message: 'Email or password is invalid' });
                }
            })
        })
    })
}

// To encrypt the password
UserSchema.pre('save', function (next) {
    let user = this;
    const costFactor = 10;

    // Hashing the password using bcrypt package
    if (user.isModified('password')) {
        bcrypt.genSalt(costFactor, (err, salt) => {
            if (!err) {
                bcrypt.hash(user.password, salt, (err, hash) => {
                    if (!err) {
                        user.password = hash;
                        next();
                    }
                })
            }
        });
    }
    else {
        next();
    }
});

const User = mongoose.model('User', UserSchema);
module.exports = { User };