const mongoose = require('mongoose');

// Tasks Schema
const TaskSchema = new mongoose.Schema({
    title: { type: String, required: true, minlength: 1, trim: true },
    _listId: { type: mongoose.Types.ObjectId, required: true },
    isCompleted: { type: Boolean, required: true, default: false }
}, { timestamps: true });

const Task = mongoose.model('Task', TaskSchema);

module.exports = { Task };