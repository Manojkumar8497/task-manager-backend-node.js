const mongoose = require('mongoose');

// Tasks Schema
const TaskListSchema = new mongoose.Schema({
    title: { type: String, required: true, minlength: 1, trim: true },
    _userId: { type: mongoose.Types.ObjectId, required: false }
}, { timestamps: true });

const TaskList = mongoose.model('TaskList', TaskListSchema);

module.exports = { TaskList };