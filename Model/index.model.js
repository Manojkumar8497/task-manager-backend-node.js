const { Task } = require('./task.model');
const { TaskList } = require('./taskList.model');

module.exports = {
    Task,
    TaskList
}