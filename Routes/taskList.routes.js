const express = require('express');
const router = express.Router();

// Conttoller
const taskListController = require('../Controller/taskList.controller');

// Authentication check
const checkAuth = require('../Middleware/authCheck');

/**
 * Get
 * Purpose: Get a single list
 */
router.get('/lists/:listId', checkAuth, taskListController.getTaskList);

/**
 * Get /lists
 * Purpose: Get all lists in database
 */
router.get('/lists', checkAuth, taskListController.getAllTaskList);

/**
 * Post /lists
 * Purpose: Create a new List
 */
router.post('/lists', checkAuth, taskListController.postTaskList);

/**
 * Patch /lists/:id
 * Purpose: To update a single list
 */
router.patch('/lists/:id', checkAuth, taskListController.updateTaskList);

/**
 * Delete /lists/:id
 * Purpose: To Delete the single List
 */
router.delete('/lists/:id', checkAuth, taskListController.deleteTaskList);

module.exports = router;