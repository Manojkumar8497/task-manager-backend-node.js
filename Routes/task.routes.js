const express = require('express');
const router = express.Router();

const taskController = require('../Controller/task.controller');

// Check Authentication
const checkAuth = require('../Middleware/authCheck');

/**
 * Get /lists/:listId
 * Purpose: To get all task from the specified list
 */
router.get('/lists/:listId', checkAuth, taskController.getAllTasks);

/**
 * Get /lists/:listId/tasks/:taskId
 * Purpose: To get the single task
 */
router.get('/lists/:listId/tasks/:taskId', checkAuth, taskController.getTask);

/**
 * Post /lists/:listId
 * Purpose: To create the new task
 */
router.post('/lists/:listId', checkAuth, taskController.createTask);

/**
 * Patch /lists/:listId/tasks/:taskId
 * Purpose: To update the task
 */
router.patch('/lists/:listId/tasks/:taskId', checkAuth, taskController.updateTask);

/**
 * Delete /lists/:listId/tasks/:taskId
 * Purpose: To Delete the task
 */
router.delete('/lists/:listId/tasks/:taskId', checkAuth, taskController.deleteTask);

module.exports = router;