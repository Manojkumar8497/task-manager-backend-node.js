const express = require('express');
const router = express.Router();
// Controller
const authController = require('../Controller/user.controller');
// Validator middleware
const checkSession = require('../Middleware/token.validator');

/**
 * POST /user
 * Purpose: To create a new user
 */
router.post('/user', authController.createUser);

/**
 * POST /login
 * Purpose: User signin
 */
router.post('/users/login', authController.login);

/**
 * GET /users/user/access-token
 * Purpose: To check the session is valid or not
 */
router.get('/users/user/access-token', checkSession, authController.getUser);

module.exports = router;