const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {

    try {
        const token = req.header('x-access-token');
        jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
            // If the jwt is not valid
            if (err) {
                throw err;
            }
            else {
                // JWT is valid
                req.userId = decoded._id;
                next();
            }
        });
    }
    catch (error) {
        error.location = "Access token checking";
        error.statusCode = 401;
        next(error);
    }

}