const { User } = require('../Model/user.model')

module.exports = async (req, res, next) => {
    try {
        const refreshToken = req.header('x-refresh-token');
        const _id = req.header('_id');

        const user = await User.findByIdAndToken(_id, refreshToken);

        if (!user) {
            throw new Error('User not found. Make sure the refresh token and user id are correct');
        }

        req.userId = user._id;
        req.refreshToken = refreshToken;
        req.userObject = user;

        let isSessionValid = false;

        user.sessions.find(session => {
            if (session.token === refreshToken) {
                if (!User.hasRefreshTokenExpired(session.expiresAt)) {
                    isSessionValid = true;
                }
            }
        });

        if (isSessionValid) {
            next();
        }
        else {
            throw new Error('Refresh token is expired or the session is invalid');
        }
    }
    catch (error) {
        error.location = 'Auth validation';
        next(error);
    }
}