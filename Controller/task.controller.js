const { Task, TaskList } = require('../Model/index.model');
const mongoose = require('mongoose');

// To get all tasks from the specified list
exports.getAllTasks = async (req, res, next) => {

    if (!req.params.listId) {
        return;
    }

    try {

        // To check the accessable
        const hasAccess = await cancreateTask(req.params.listId, req.userId);

        if (hasAccess) {
            const tasks = await Task.find({ _listId: req.params.listId });
            res.status(200).send(tasks);
        }
        else {
            const error = new Error();
            error.status = 404;
            error.message = "Not found, Please try again."
            throw error;
        }
    }
    catch (error) {
        error.location = 'Get all tasks';
        next(error);
    }

}

// To get Single Task
exports.getTask = async (req, res, next) => {

    if (!req.params.listId || !req.params.taskId) {
        return;
    }

    try {

        // To check the accessable
        const hasAccess = await cancreateTask(req.params.listId, req.userId);

        if (hasAccess) {
            const task = await Task.findOne({ _id: req.params.taskId, _listId: req.params.listId });
            res.status(200).send(task);
        }
        else {
            const error = new Error();
            error.status = 404;
            error.message = "Not found, Please try again."
            throw error;
        }
    }
    catch (error) {
        error.location = 'Get a single task';
        next(error);
    }

}

// To create a new task from the speciied
exports.createTask = async (req, res, next) => {

    if (!req.params.listId) {
        return;
    }

    try {

        // To check the accessable
        const hasAccess = await cancreateTask(req.params.listId, req.userId);

        if (hasAccess) {
            const task = new Task({
                title: req.body.title,
                _listId: mongoose.Types.ObjectId(req.params.listId)
            })

            const response = await task.save();
            res.status(200).send(response);
        }
        else {
            const error = new Error();
            error.status = 404;
            error.message = "Tasklist not found, Please try again."
            throw error;
        }

    }
    catch (error) {
        error.location = 'Create new task';
        next(error);
    }

}

// To update the specified task
exports.updateTask = async (req, res, next) => {

    // console.log(req.params, req.body)
    if (!req.params.taskId || !req.body) {
        return;
    }

    try {

        // To check the accessable
        const hasAccess = await cancreateTask(req.params.listId, req.userId);

        if (hasAccess) {
            const response = await Task.findOneAndUpdate({ _id: req.params.taskId, _listId: req.params.listId }, { $set: req.body });
            res.status(200).send(response);
        }
        else {
            const error = new Error();
            error.status = 404;
            error.message = "Not found, Please try again."
            throw error;
        }

    }
    catch (error) {
        error.location = 'Update the specified task';
        next(error);
    }

}

// To delete the specified task
exports.deleteTask = async (req, res, next) => {

    if (!req.params.taskId) {
        return;
    }

    try {

        // To check the accessable
        const hasAccess = await cancreateTask(req.params.listId, req.userId);

        if (hasAccess) {
            const response = await Task.findOneAndDelete({ _id: req.params.taskId, _listId: req.params.listId });
            res.status(200).send(response);
        }
        else {
            const error = new Error();
            error.status = 404;
            error.message = "Not found, Please try again."
            throw error;
        }

    }
    catch (error) {
        error.location = 'Delete the specified task';
        next(error);
    }

}

/** Helper method */

// To check the currently loggedin user has access to CURD
const cancreateTask = async (listId, userId) => {
    const taskList = await TaskList.find({ _id: listId, _userId: userId });

    if (taskList.length > 0) {
        return true;
    }
    else {
        return false;
    }
}