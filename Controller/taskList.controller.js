const { TaskList, Task } = require('../Model/index.model');

// To get a single list
exports.getTaskList = async (req, res, next) => {
    if (!req.params.listId) {
        return;
    }
    try {
        const taskLists = await TaskList.findOne({ _id: req.params.listId, _userId: req.userId });
        res.status(200).send(taskLists);
    }
    catch (error) {
        error.location = "Get single list";
        next(error);
    }
}

// To get all task lists from database
exports.getAllTaskList = async (req, res, next) => {
    try {
        const taskLists = await TaskList.find({ _userId: req.userId });
        res.send(taskLists);
    }
    catch (error) {
        error.location = 'Get all task list';
        next(error);
    }
}

// To post the task list to database
exports.postTaskList = async (req, res, next) => {
    if (!req.body.title) {
        return;
    }

    const title = req.body.title;

    try {
        const tasklist = new TaskList({
            title: title,
            _userId: req.userId
        })
        const taskList = await tasklist.save();
        res.send(taskList);
    }
    catch (error) {
        error.location = 'create a new task list';
        next(error);
    }
}

// To Update the single task list
exports.updateTaskList = async (req, res, next) => {

    if (!req.params.id || !req.body.title) {
        return;
    }

    const listId = req.params.id;

    try {
        const response = await TaskList.findOneAndUpdate({ _id: listId, _userId: req.userId }, { $set: req.body });
        res.send(response);
    }
    catch (error) {
        error.location = 'Update the single task list';
        next(error);
    }
}

// To Delete the single task list
exports.deleteTaskList = async (req, res, next) => {
    console.log(req.params)
    if (!req.params.id) {
        return;
    }

    try {
        const response = await TaskList.findOneAndDelete({ _id: req.params.id, _userId: req.userId });

        // Delete all the task that are in the deleted list
        if (response) {
            deleteTasks(response._id);

            res.send(response);
        }
        else {
            const error = new Error();
            error.message = "Task list not found, please try again.";
            error.status = 404;
            throw error;
        }
    }
    catch (error) {
        error.location = 'Delete the single task list';
        next(error);
    }

}

/** Helper Methods */
const deleteTasks = async (_listId) => {
    await Task.deleteMany({ _listId });
    console.log(`Task from ListId: ${_listId} were deleted`);
}