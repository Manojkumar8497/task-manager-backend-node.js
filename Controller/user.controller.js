const { User } = require('../Model/user.model');

// To Register the new user to database
exports.createUser = async (req, res, next) => {
    if (!req.body.email || !req.body.password) {
        return;
    }

    try {
        let newUser = new User(req.body);
        // To save and get the user details
        const user = await newUser.save();
        // To create a refresh token
        const refreshToken = await newUser.createSession(req.device.name, req.device.type);
        // To create a AccessToken
        const accessToken = await newUser.generateAccessAuthToken();

        res
            .header('x-refresh-token', refreshToken)
            .header('x-access-token', accessToken)
            .send(user);
    }
    catch (error) {
        error.location = 'Register the new user to database';
        next(error);
    }

}

// For login
exports.login = async (req, res, next) => {

    if (!req.body.email || !req.body.password) {
        return;
    }

    try {
        // To check the password is correct or not
        const user = await User.findByCredentials(req.body.email, req.body.password);
        // To create the refresh token
        const refreshToken = await user.createSession(req.device.name, req.device.type);
        // To create the access token
        const accessToken = await user.generateAccessAuthToken();
        // To send the auth tokens in response header
        res
            .status(200)
            .header('x-refresh-token', refreshToken)
            .header('x-access-token', accessToken)
            .send(user);
    }
    catch (error) {
        error.location = 'For login';
        next(error);
    }
}

// To get the currently logged user details
exports.getUser = async (req, res, next) => {

    try {
        const accessToken = await req.userObject.generateAccessAuthToken();
        res.header('x-access-token', accessToken).send({ accessToken });
    }
    catch (error) {
        error.location = 'Create a access token based on refresh token';
        next(error);
    }

} 