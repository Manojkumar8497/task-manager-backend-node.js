const express = require('express');
const app = express();

// To detect the use device
const device = require('express-device');

// Morgon
const morgan = require('morgan');

// DB initialize
const { mongoose } = require('./DB/mongoose');

// To initialize the .env file
require('dotenv').config();

// Server port listening
const PORT = process.env.PORT | 3000;

// MiddleWare
const bodyParser = require('body-parser');

// Router Path
const taskListRouter = require('./Routes/taskList.routes');
const taskRouter = require('./Routes/task.routes');
const userRouter = require('./Routes/user.routes');

// Morgan
app.use(morgan('dev'));

// MiddleWare
app.use(bodyParser.json());

// Express device capture
app.use(device.capture({parseUserAgent: true}));

// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS, PUT, HEAD");
    res.header("Access-control-Allow-Headers", "Origin, X-Requested-With, x-access-token, x-refresh-token, _id, Content-Type, Accept");

    // To expose the headers to front-end
    res.header("Access-Control-Expose-Headers", "x-access-token, x-refresh-token");

    next();
})

// Tasks Routes
app.use('/api/taskList', taskListRouter);
app.use('/api/task', taskRouter);
app.use('/api/auth', userRouter);

// Error Handling middleware
app.use((error, req, res, next) => {
    const message = error.message;
    const statusCode = error.statusCode || 500;
    const errLocation = error.location || 'Unidentified path';
    res.status(statusCode).json({ message: message, path: errLocation });
})

app.listen(PORT, () => {
    console.log("Server is listening to the port: " + PORT);
});