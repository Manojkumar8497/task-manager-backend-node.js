// For the MongoDB connection
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/TaskManager', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log("Database connection Successfully");
    })
    .catch(err => {
        console.log("MongoDB Connection Failed:");
        console.log(err.message);
    });

// To prevent deperication
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', true);

module.exports = {
    mongoose
}